<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Collabrains</title>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@700&display=swap" rel="stylesheet">
     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        *{
            box-sizing: border-box;
            padding: 0;
            margin: 0;
            font-family: 'Raleway',sans-serif;
            background: #78C0E0;
        
        }
        .btn{
            padding: 0.5rem 1.5rem;
            background: #51306b;
            color: white;
            display: inline-block;
            font-family: 'Raleway',sans-serif;
            font-weight: 700;
            font-size: 1.1rem;
        }
        a{
            text-decoration: none;
        }
        .login{
            width: 60%;
            margin: auto;
        }

        .loginimage{
            height: 851px;
            width: 1000px;
        }
        .login-form{
            margin: 2rem;
        }
        .logo-line{
            margin-bottom: 2rem;
            display: inline;
        }
        .collabrains{
            margin-bottom: 2rem;
        }
        .l-heading{
            font-family: 'Raleway', sans-serif;
            font-size: 2.2rem;
            color: #51306b;
        }
        .purple{
            color:#92267a;
        }
        .form-heading{
            margin: 1.5rem 0;
            font-size: 1.8rem;
            font-family: 'Raleway',sans-serif;
            color: #51306b;
            text-align: center;
        }
        .form{
            margin: 1.5rem 0;
        }
        label{
            display: block;
            font-size: 1.2rem;
            color: #92267a;
            font-family: 'Raleway',sans-serif;
            margin-bottom: 0.7rem;
        }
        input{
            display: block;
            margin-bottom: 1rem;
            border: none;
            border-bottom: 1px solid#92267a;
            width: 100%;
            height: 2rem;
        }

        .submit{
            margin-bottom: 0;
        }

        input:focus{
            outline: none;
        }
        ::placeholder{
            color: #92267a;
        }
        .forgot{
            display: inline;
            margin-left: 12.5rem;
            opacity: 0.7;
            font-size: 1rem;
            color: #92267a;
        }
        .a-divider-break{
            text-align: center; /* this brings New Here to center*/
            position: relative;/*this is for positioning*/
            top: 2px;
            padding-top: 1px;
            margin-bottom: 14px;
            line-height: 0;
            margin-top: 14px;
        }
        .a-divider-break h5{
            line-height: 1;
            font-size: 12px;
            color:#92267a;
            font-weight: 400;
            z-index: 2;
            position: relative;
            display: inline-block;/*this is to make a block of white color*/
            /* background-color: white; */
            padding: 0 8px 0 8px;
        }
        .a-divider-break:after{
            /*this thing creates a grey line of new here*/
            content: "";/*necessary whenever u use :before or :after*/
            width: 100%;
            background-color: transparent;
            display: block;
            height: 1px;
            border-top: 1px solid #92267a;
            position: absolute;
            top: 50%;/*using this the line is brought up*/
            margin-top: -1px;
            z-index: 1;
        }
        .signup-button{
            width: 100%;
            text-align: center;
            margin-top: 1rem;
        }

        .heading{
                font-size: 48px;
                text-align: center; 
                font-family: 'Agent Red', arial;
                color: #92267a;
                margin-bottom: 40px;
        }

        .year_dropdown{
            display: inline-block;
        }
    </style>
</head>
<body>
    <section class="mainpage">
        <div class="login">
            <div class="login-form">
                <div class="heading">Collabrains</div>
                <h2 class="form-heading">Signup</h2>
                <form class="form" method="POST" action="{{ route('register') }}">
                    @csrf   
                    <label for="name">Name</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    <label for="id">Email ID</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    <label for="college_name">College Name</label>
                    <input id="college_name" type="text" class="form-control @error('college_name') is-invalid @enderror" name="college_name" value="{{ old('college_name') }}" required autofocus>
                    <div class="year_dropdown">
                        <span>
                            <label for="year">Year</label>
                        </span>
                        {{-- <input id="year" type="text" class="form-control @error('year') is-invalid @enderror" name="year" value="{{ old('year') }}" required autofocus> --}}
                        <span>
                            <select name="year" id="year">
                                <option value="First Year">First Year</option>
                                <option value="Second Year">Second Year</option>
                                <option value="Third Year">Third Year</option>
                                <option value="Fourth Year">Fourth Year</option>
                            </select>
                        </span>
                    </div>
                    <label for="branch">Branch</label>
                    <input id="branch" type="text" class="form-control @error('branch') is-invalid @enderror" name="branch" value="{{ old('branch') }}" required autofocus>
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    <label for="password-confirm">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"><br><br>
                    <input style="cursor: pointer;" type="submit" name="submit" class="btn button-primary submit">
                </form>
                 <div class="a-divider a-divider-break">
                    <h5>Already Have an Account?</h5>
                </div>
                <a href="/login" class="btn signup-button">Sign In</a>
            </div>
        </div>
    </section>
</body>
</html>