 <Style>
    .nav-link{
      color: white;
    }

    /* .nav-link :enabled{
      color: #92267a;
    } */
  </Style>
  
  <nav class="navbar navbar-expand-md shadow-sm" style="background-color: #013438">
    <div class="container">
        <a class="navbar-brand nav-link" href="{{ url('/') }}" style="font-size: 20px">
           Collabrains
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>
            <ul class="navbar-nav mr-auto">
              {{-- <li class="nav-item">
                <a class="nav-link" href="/about">About</a>
              </li> --}}
              {{-- <li class="nav-item">
                <a class="nav-link" href="/services" >Services</a>
              </li> --}}
            </ul>
            @guest
            @else
            <ul class="nav navbar-nav navbar-right">

              
            {{-- 
              <li class="nav-item dropdown">
                <a id="navbarDropdown-1" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    Categories
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown-1" style="background-color: bisque" >
                  <a class="dropdown-item" href="/profile">
                    Profile
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                  </form>
                </div>
             </li> --}}

              <li class="nav-item">
                <a class="nav-link" href="/posts">
                  <i class="fa fa-home" aria-hidden="true" style="font-size: 18px"></i>&nbsp;&nbsp;Home</a>
              </li>
              &nbsp; &nbsp; &nbsp;
              <li class="nav-item">
                <a class="nav-link" href="/posts/create" >
                  <i class="fa fa-pencil" aria-hidden="true" style="font-size: 16px"></i>&nbsp;&nbsp;Ask A Question</a>
              </li>
            </ul>
            @endguest

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif
                    
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          <i class="fa fa-user" aria-hidden="true" style="font-size: 16px"></i>&nbsp;&nbsp;{{ Auth::user()->name }}
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background-color: bisque" >
                          <a class="dropdown-item" href="/profile">
                            Profile
                          </a>
                          <a class="dropdown-item" href="/dashboard">
                            Dashboard
                          </a>  
                          <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                          </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>