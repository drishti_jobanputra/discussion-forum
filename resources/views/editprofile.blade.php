@extends('layouts.app')

@section('content')
    <style>
        body{
            background-color: #d7ffab;
        }
                
        .edit{
            width: 60%;
            margin: auto;
        }

        .submit-changes{
            margin-bottom: 8px;
            margin-top: -18px;
            padding: 8px;
            padding-left: 16px;
            padding-right: 16px;
            color: white;
            background-color: #51306b;
            border: none;
            border-radius: 4px;
            float: right;
        }

    
    </style>
        <!-- Edit profile -->

        <h2 class="heading" style="margin-bottom: 16px; margin-top: 8px;">Edit Profile</h2>
        <div class="edit container">
            <form action="/profile/update" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="college_name">College Name:</label>
                    <input type="text" class="form-control" name="college_name" id="college_name" value="{{$user->college_name}}">
                </div>
                <div class="form-group">
                    <label for="year">Year:</label>
                    <select name="year" id="year" value="{{$user->year}}">
                        @if ($user->year == "First Year")
                            <option value="First Year" selected>First Year</option>
                        @else 
                            <option value="First Year">First Year</option>
                        @endif

                        @if ($user->year == "Second Year")
                            <option value="Second Year" selected>Second Year</option>
                        @else 
                            <option value="Second Year">Second Year</option>
                        @endif

                        @if ($user->year == "Third Year")
                            <option value="Third Year" selected>Third Year</option>
                        @else 
                            <option value="Third Year">Third Year</option>
                        @endif

                        @if ($user->year == "Fourth Year")
                            <option value="Fourth Year" selected>Fourth Year</option>
                        @else 
                            <option value="Fourth Year">Fourth Year</option>
                        @endif
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="branch">Branch:</label>
                    <input type="text" class="form-control" name="branch" id="branch" value="{{$user->branch}}">
                </div>
                    <h6 style="margin-top: 16px; margin-bottom: 16px;">Password Change:</h6>
                    <div class="pass-change">
                        <div class="form-group col-md-6" style="padding-left: 0 !important;">
                            <label for="old_pass">Old Password</label>
                            <input type="password" name="old_pass" class="form-control" id="old_pass">
                            </div>
                            <div class="form-group col-md-6" style="padding-left: 0 !important;">
                            <label for="password">New Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group col-md-6" style="padding-left: 0 !important;">
                            <label for="password-confirm">Re-enter New Password</label>
                            <input type="password" class="form-control" id="password-confirm" name="password_confirmation">
                        </div>
                    </div>
                 <input type="submit" name='submit' submit="Save changes" class="submit-changes">
               
            </form>
            
        </div>

    </section>

@endsection