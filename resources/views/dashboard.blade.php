@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#12bac5;
        color: #51306b;
        
    }
    .well{
        color: #3490dc;
        margin: 16px 0;
        padding: 16px;
        background-color: azure;
        border-radius:8px; 
    }

    .well h3 a{
        color: #3490dc;/* #931621; 473198*/
    }

    .writtenby{
        font-size: 12px;
        color: darkslategray;
        text-align: right;
    }

    .q_cat{
        float: right;
        color: #51306b;
        font-size: 12px;
    }

    .explore-cat{
        width: 50%; 
        margin: 16px auto;       
        color: #3490dc;
        padding: 16px;
        background-color: azure;
        border-radius:8px; 
        text-align: center;
    }

</style>
<body>
    <h1 class="heading">Dashboard</h1>
        <h2 style="font-weight: 900;">Asked Questions</h2>
        @if(count($user->posts) > 0)
                @foreach($user->posts as $post)
                    <a href="/posts/{{$post->id}}">
                        <div class="well">
                            <div class="q_cat">Category: {{$post->category->name}}</div>
                            <a href="/posts/{{$post->id}}">
                                <div style="max-height:24px; white-space: nowrap; width:1024px; overflow:hidden; text-overflow:ellipsis; ">
                                    {{$post->body}}...
                                </div>
                            </a>
                            <div class="writtenby">Written on {{$post->created_at}} by <strong>{{$post->user->name}} </strong></div>
                                <hr>
                            <a href="/posts/{{$post->id}}/reply" class="btn btn-default" style="background-color: #51306b; color: white; font-size:12px">
                                <i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 15px"></i>
                                Answer
                            </a><br>
                        </div>
                    </a>
                @endforeach
        @else
            <p>No asked questions</p>
        @endif

        <h2 style="font-weight: 900;">Answered Questions</h2>
        @if(count($user->replies) > 0)
                @foreach($user->replies as $reply)
                    <a href="/posts/{{$reply->post_id}}">
                        <div class="well">
                            <div class="q_cat">Category: {{$reply->post->category->name}}</div>
                            <p style="max-height:24px; white-space: nowrap; width:1024px; overflow:hidden; text-overflow:ellipsis; ">{{$reply->post->body}}...</p>
                            <div class="writtenby">Written on {{$reply->post->created_at}} by <strong>{{$reply->post->user->name}} </strong></div>
                        </div>
                    </a>
                @endforeach
        @else
            <p>No answered questions</p>
        @endif

        <h2 style="font-weight: 900;">Categories You Follow</h2>
        @if(count($user->followings) > 0)
                @foreach($user->followings as $following)
                            <div class="explore-cat">
                                {{$following->category->name}}
                            </div>
                @endforeach
        @else
            <p>No Categories Followed</p>
        @endif
    @endsection
</body>