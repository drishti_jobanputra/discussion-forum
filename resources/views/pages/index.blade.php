@extends('layouts.app')

@section('content')
<link href="https://allfont.net/allfont.css?fonts=agent-red" rel="stylesheet" type="text/css" />

<style>
        body{
                color: white;
                background-color: #6050dc;
        }

        .heading{
                font-size: 72px;
                text-align: center; 
                font-family: 'Agent Red', arial;
                color: #e2d92f;
                margin-bottom: 40px;
        }

        .contents{
                margin-top: 50px;
        }

        .btn{
                padding: 0.5rem 1.5rem;
                background: #2F004F;
                color: white;
                display: inline-block;
                font-family: 'Raleway',sans-serif;
                font-weight: 700;
                font-size: 1.1rem;
        }

        .btn:hover{
                color:#e2d92f;
        }

</style>
        <div class="contents">
                <div class="heading">Collabrains</div>
                <br>
                <div class="grid-item-content"style="font-size: 26px;">
                        <p style="text-align: center;">This is an online platform open for all Engineering students to ask their doubts related to their subjects as well as answer their fellow students' doubts.
                        Our aim is to connect all the Engineering students on a single platform where they can discuss about their doubts and clear them.
                        Various categories of questions have already been provided for convenience!</p>
                        <p style="text-align: center">To get started, just register in a few easy steps!</p>
                        @guest
                        <div style="text-align: center">
                                <a href="/login" class="btn login-button" style="margin-left: 0">Login</a>
                                <a href="/register" class="btn register-button">Register</a>
                        </div>
                        @endguest
                </div>
        </div>
@endsection