@extends('layouts.app')

@section('content')
    <style>

        body{

            background-color: #d7ffab;
        }

        .profile-grid{
            padding: 32px;
            display: grid;
            grid-template-columns: auto;
            grid-row-gap: 32px;
            width: 50%;
            margin: auto;
            font-size: 18px;
        }

        .profile{
        font-size: 20px;
        }

        .profile-grid{
            padding: 32px;
            display: grid;
            grid-template-columns: auto;
            grid-row-gap: 32px;
            width: 50%;
            margin: auto;
            font-size: 18px;
        }

        .edit-button{
            margin-top: 16px;
            margin-bottom: 16px;
            color: white;
            background-color: #51306b;
            border: none;
            border-radius: 4px;
            padding: 4px;
            padding-left: 16px;
            padding-right: 16px;
        }

        .profile-grid p{
            margin: 0;
            font-weight: bold;
            color:  #51306b;
        }

    </style>

<h2 class="heading" style="margin-bottom: 16px; margin-top: 16px;">User Profile</h2>
<div class="profile-grid">
    <div class="profile-name">
        <p>Name:</p>
        <div class="display-name">
            {{$user->name}}
        </div>
    </div>
    <div class="email-add">
        <p>Email Address:</p>
        <div class="display-email">
            {{$user->email}}
        </div>
    </div>
    <div class="c-name">
        <p>College Name:</p>
        <div class="display-c-name">
            {{$user->college_name}}
        </div>
    </div>
    <div class="year">
        <p>Year:</p>
        <div class="display-year">
            {{$user->year}}
        </div>
    </div>
    <div class="branch">
        <p>Branch:</p>
        <div class="display-branch">
            {{$user->branch}}
        </div>
    </div>
    <a href="/profile/edit">
        <button class="edit-button" style="float: right; cursor: pointer;">Edit</button>
    </a>
</div>
@endsection