@extends('layouts.app')

@section('content')
<style>
    body{
        background-color: rgb(248, 148, 136);
        
    }

    .btn{
        background-color: #51306b;
        color: white;
    }


</style> 
    <h1 class="heading" style="margin-bottom: 32px;">Ask A Question</h1>
    {!! Form::open(['action' => '\App\Http\Controllers\PostsController@store', 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::label('category','Category:')}}
            {{Form::select('category', $cats, null, ['placeholder' => 'Select a Category'])}}
        </div>

        <div class="form-group">
            {{Form::label('body','Question:')}}
            {{Form::textarea('body', '', ['id'=>'article-ckeditor','class' => 'form-control', 'placeholder' => 'Question' ])}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn'])}}
    {!! Form::close() !!}
@endsection