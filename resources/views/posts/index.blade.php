@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#12bac5;
        color: white;
    }
    .well{
        color: #3490dc;
        margin: 16px 0;
        padding: 16px;
        background-color: azure;
        border-radius:8px; 
    }
    .well h3 a{
        color:#ffffff;/* #931621; 473198*/
    }

    .writtenby{
        font-size: 12px;
        color: darkslategray;
        text-align: right;
    }

    hr{
        margin: 8px;
    }

    .explore-cat{
        width: 50%; 
        margin: 16px auto;       
        color: #3490dc;
        padding: 16px;
        background-color: azure;
        border-radius:8px; 
        text-align: center;
    }

    .q_cat{
        float: right;
        color: #51306b;
        font-size: 12px;
    }

    .explore-head{
        text-align: center; 
        font-family: 'Agent Red', arial; 
        color: #92267a;
    }

    #HeadingTwo{
        background-color: #12bac5;
        text-decoration: none;
    }

    .down-angle{
        font-size: 60px;
        vertical-align: middle;
    }

    .card{
        border: none;
        width: 50%;
        margin: auto;
    }

</style>
<body>
    <div cless="container">
        <div id="accordion">
            <div class="card">
                    <button id="headingTwo" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <h1 class="explore-head">Explore <i class="fa fa-angle-down down-angle" aria-hidden="true"></i></h1>
                    </button>
                </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        @if(count($cats) > 0)
                            @foreach($cats as $cat)
                                <a href="/categories/{{$cat->id}}">
                                    <div class="explore-cat">
                                            {{$cat->name}}
                                    </div>
                                </a>
                            @endforeach
                            @else
                            <p>No Category found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="heading">Posts</h1>
        @if(count($user->followings) > 0)
            @foreach($user->followings as $following)
                @foreach($following->category->posts as $post)
                        <div class="well">
                            <div class="q_cat">Category: {{$post->category->name}}</div>
                            <a href="/posts/{{$post->id}}">
                                <div style="max-height:24px; white-space: nowrap; width:1024px; overflow:hidden; text-overflow:ellipsis; ">
                                    {{$post->body}}...
                                </div>
                            </a>
                            <div class="writtenby">Written on {{$post->created_at}} by <strong>{{$post->user->name}} </strong></div>
                                <hr>
                            <a href="/posts/{{$post->id}}/reply" class="btn btn-default" style="background-color: #51306b; color: white; font-size:12px">
                                <i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 15px"></i>
                                Answer
                            </a><br>
                        </div>
                @endforeach
            @endforeach
        @else
            <p>No post found</p>
        @endif
    </div>

    @endsection
</body>