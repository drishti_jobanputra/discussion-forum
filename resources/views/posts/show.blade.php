@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#f7baed;
        color: black;
    }

    .btn{
        background-color: #51306b;
        color: white;
    }

    .question{
        margin-top: 24px;
    }

    .writtenby{
        font-size: 12px;
        color: darkslategray;
        text-align: right;
    }

    .writtenreply{
        font-size: 12px;
        color: darkslategray;
    }


</style> 
    <a href="/posts" class="btn btn-default" style="float: right;">Go Back</a><br><br>
    <div class="q_cat">Category: {{$post->category->name}} </div>
    @if($follow)
        <div> 
            {!! Form::open(['action' => '\App\Http\Controllers\FollowController@store', 'method' => 'POST']) !!}
            {{Form::hidden('cat_id', $post->category_id)}}
            {{Form::submit('Follow', ['class' => 'btn btn '])}}
            {!! Form::close() !!}
        </div>
    @endif
    <div class="question">
        {!!$post->body!!}
    </div>
    <div class="writtenby">Written on {{$post->created_at}} by <strong>{{$post->user->name}}</strong> </div>

    @if(Auth::check())
        @if(Auth::user()->id == $post->user_id)
        <hr>
            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a><br><br>

            {!!Form::open(['action' => ['\App\Http\Controllers\PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right' ])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif

    <a href="/posts/{{$post->id}}/reply" class="btn btn-default" style="background-color: #51306b; color: white; font-size:12px">
        <i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 15px"></i>
        Answer
    </a>

    

    <hr>

    @if(count($post->replies)>0)
        <div>
            <h6>Answers</h6>
            <hr>            
            @foreach($post->replies as $reply)
            <div class="writtenreply">Written On {{$reply->created_at}} <br> by <strong>{{$reply->user->name}} </strong></div>
                <br>
                {{$reply->reply_content}}
                <hr>
            @endforeach
        </div>
    @endif
    


@endsection