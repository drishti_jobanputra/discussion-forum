@extends('layouts.app')

@section('content')
<style>
    body{
        background-color:#12bac5;
        color: white;
    }
    .well{
        color: #3490dc;
        margin: 16px 0;
        padding: 16px;
        background-color: azure;
        border-radius:8px; 
    }
    .well h3 a{
        color:#ffffff;/* #931621; 473198*/
    }

    .btn{
        background-color: #51306b;
        color: white;
    }

    .writtenby{
        font-size: 12px;
        color: darkslategray;
        text-align: right;
    }

</style>
<body>
        <h1 class="heading">{{$cat->name}}</h1>
        @if($follow)
            <div> 
                {!! Form::open(['action' => '\App\Http\Controllers\FollowController@store', 'method' => 'POST']) !!}
                {{Form::hidden('cat_id', $cat->id)}}
                {{Form::submit('Follow', ['class' => 'btn btn '])}}
                {!! Form::close() !!}
            </div>
        @endif
        @if(count($cat->posts) > 0)
                @foreach($cat->posts as $post)
                    <a href="/posts/{{$post->id}}">
                        <div class="well">
                            <p style="max-height:24px; white-space: nowrap; width:1024px; overflow:hidden; text-overflow:ellipsis; ">{{$post->body}}...</p>
                            <div class="writtenby">Written on {{$post->created_at}} by <strong>{{$post->user->name}} </strong></div>
                            <a href="/posts/{{$post->id}}/reply" class="btn btn-default" style="background-color: #51306b; color: white;">Answer</a><br><br>
                        </div>
                    </a>
                @endforeach
        @else
            <p>No post found</p>
        @endif
    @endsection
</body>