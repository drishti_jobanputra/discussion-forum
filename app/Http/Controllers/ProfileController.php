<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('Profile')->with('user', $user);
    }

    public function edit()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('EditProfile')->with('user', $user);
    }

    public function update(Request $request)
    {
        $this -> validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'college_name' => ['required', 'string', 'max:200'],
            'year' => ['required', 'string', 'max:25'],
            'branch' => ['required', 'string', 'max:100'],
            'old_pass' => ['required', 'string', 'min:8'],
        ]);
        
       $user=User::find(auth()->user()->id);
       $pass = $request->input('old_pass');
       if (Hash::check($pass, $user->password))
            {
                $user->name = $request->input('name');
                $user->college_name = $request->input('college_name');
                $user->branch = $request->input('branch');
                $user->year = $request->year;
                $user->password = Hash::make($request->input('password'));
                $user->save();
                return redirect('/profile')->with('success','Profile Updated');
            }

        else 
            {
                return redirect('/profile/edit')->with('error','Old Password does not match');
            }
    // echo $request;
    }

}
